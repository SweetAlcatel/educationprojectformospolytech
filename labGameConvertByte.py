import random


def Child(value):
    if (value > '00000111'):
        print('Измените сложность')
    else: 
        randomValue = random.randint(0, 1)
        if(randomValue == 0):
            print(int(value, base=2))
            print('Сыграйте снова!')
        else:
            print(hex(int(value)))
            print('Сыграйте снова!')

def Easy(value):
    if (value > '00001111'):
        print('Измените сложность')
    else: 
        randomValue = random.randint(0, 1)
        if(randomValue == 0):
            print(int(value, base=2))
            print('Сыграйте снова!')
        else:
            print(hex(int(value)))
            print('Сыграйте снова!')

def Medium(value):
    if (value > '00111111'):
        print('Измените сложность')
    else: 
        randomValue = random.randint(0, 1)
        if(randomValue == 0):
            print(int(value, base=2))
            print('Сыграйте снова!')
        else:
            print(hex(int(value)))
            print('Сыграйте снова!')

def Hard(value):
    if (value > '11111111'):
        print('Измените сложность') 
    else: 
        randomValue = random.randint(0, 1)
        if(randomValue == 0):
            print(int(value, base=2))
            print('Сыграйте снова!')
        else:
            print(hex(int(value)))
            print('Сыграйте снова!')

hard_level = {
    'child': Child,
    'easy': Easy,
    'medium': Medium,
    'hard': Hard
}

hard_level['child']('00000001')

