# width - длина подмассива - диапазон по таблице от 65 до 128 
# height - количество строк в  общей матрице

def Frame(maxWidth, height):
    array = []

    for _ in range(height):
        array.append([chr(item) for item in range(65, maxWidth)])

    for row in array:
        for columns in row:
            print(columns, end=' ')
        print()

Frame(128, 3)